package it.dev.springbootapi.api;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.dev.springbootapi.objects.ObjectToValidate;
import it.dev.springbootapi.objects.User;
import it.dev.springbootapi.services.IUserService;

@RestController
@RequestMapping("/users")
@Validated
public class UsersController {

	private static final Logger logger = LoggerFactory.getLogger(UsersController.class);
	@Autowired
	private IUserService userService;
	
	 @RequestMapping("/all")
	public List<User> getUsers() {
		 logger.trace("chiamata a users/all");
		return userService.getUsers();
	}
	 
	 //FUNZIONA SOLO SE SI METTE il @Validated in cima al controller! Inoltre l'errore di validazione
	 //è diverso: viene reso un 500 (internal server error) e non un 400 (bad request)
	 /* es. {
	    "timestamp": "2020-03-21T14:24:53.843+0000",
	    "status": 500,
	    "error": "Internal Server Error",
	    "message": "validationGet.second: must be greater than or equal to 2",
	    "path": "/users/validationGet"
	}
	*/
	 @GetMapping("/validationGet")
		public ResponseEntity<String> validationGet(@NotBlank(message="first mandatory") String first, @NotNull @Min(2) Integer second) {
		 return ResponseEntity.ok("first is valid in get");
		}
	 
	 //FUNZIONA solo se ci si mette il @Valid (il @Validated in alto non serve in questo caso):
	 // SI DEVE METTERE I GET E I SET SULLE PROPERTIES DELL'OGGETTO SENNO' DA' SEMPRE VALIDAZIONE ERRATA
	 @PostMapping("/otv")
	 ResponseEntity<String> postOTV(@Valid @RequestBody ObjectToValidate obj) {
		 // persisting the user
		 return ResponseEntity.ok("OTV is valid");
	 }
	 
	//FUNZIONA solo se ci si mette il @Valid (il @Validated in alto non serve in questo caso): 
	 //SI DEVE TOGLIERE IL @RequestParam (SpringBoot mappa da solo le props sulla base del nome dei parametri) 
	 //e METTERE I GET E I SET SULLE PROPERTIES DELL'OGGETTO SENNO' DA' SEMPRE VALIDAZIONE ERRATA.
	 //Dà errore di validazione 400 es:
	 /*
	  * {
    "timestamp": "2020-03-21T14:31:23.332+0000",
    "status": 400,
    "error": "Bad Request",
    "errors": [
        {
            "codes": [
                "NotBlank.objectToValidate.first",
                "NotBlank.first",
                "NotBlank.java.lang.String",
                "NotBlank"
            ],
            "arguments": [
                {
                    "codes": [
                        "objectToValidate.first",
                        "first"
                    ],
                    "arguments": null,
                    "defaultMessage": "first",
                    "code": "first"
                }
            ],
            "defaultMessage": "first is mandatory",
            "objectName": "objectToValidate",
            "field": "first",
            "rejectedValue": "  ",
            "bindingFailure": false,
            "code": "NotBlank"
        }
    ],
    "message": "Validation failed for object='objectToValidate'. Error count: 1",
    "path": "/users/otv"
}
	  */
	 @GetMapping("/otv")
	 ResponseEntity<String> getUser(@Valid ObjectToValidate obj) {
		 // persisting the user
		 return ResponseEntity.ok("OTV is valid");
	 }
	 
}
