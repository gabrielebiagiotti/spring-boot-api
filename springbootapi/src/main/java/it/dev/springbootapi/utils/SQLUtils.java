package it.dev.springbootapi.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

public class SQLUtils {
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String CONNECTION = "jdbc:mysql://85.10.205.173:3306/biagiomysql";
	private static final String USER = "biagiomysql";
	private static final String PASSWORD = "biagiomysql";
	private SQLUtils() {
	}
	static
	{
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Connection initConnection() throws SQLException
	{
		return DriverManager.getConnection(CONNECTION, USER, PASSWORD);
	}
	
	public static void closeConnection(Connection c)
	{
		if(c!=null)
			try {
				c.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
}
