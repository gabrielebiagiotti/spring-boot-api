package it.dev.springbootapi.utils;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.jdbc.pool.DataSourceFactory;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.embedded.tomcat.TomcatWebServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiObjectFactoryBean;

@Configuration
public class TomcatConfiguration {

	@Value("${app.datasource.url}")
	private String dbUrl;
	
	@Value("${app.datasource.username}")
	private String dbUsername;
	
	@Value("${app.datasource.password}")
	private String dbPsw;
	
	@Value("${app.datasource.driver-class-name}")
	private String dbDriver;
	
	@Bean
	 public TomcatServletWebServerFactory  tomcatFactory() {
	        
		 return new TomcatServletWebServerFactory () {
	          
			 @Override
		        protected TomcatWebServer getTomcatWebServer(Tomcat tomcat) {
		            tomcat.enableNaming(); 
		            return super.getTomcatWebServer(tomcat);
		        }
	        	
			 @Override 
		        protected void postProcessContext(Context context) {

		            ContextResource resource = new ContextResource();
		            resource.setName("jdbc/apiDS");
		            resource.setType(DataSource.class.getName());
		            resource.setProperty("driverClassName", dbDriver);
		            resource.setProperty("url", dbUrl);
		            resource.setProperty("username", dbUsername);
		            resource.setProperty("password", dbPsw);
//		            "org.apache.tomcat.jdbc.pool.DataSourceFactory"
		            resource.setProperty("factory", DataSourceFactory.class.getName());
		            context.getNamingResources().addResource(resource);          
		        }
	        };
	    }
	 
	    @Bean
	    public DataSource jndiDataSource() throws IllegalArgumentException, NamingException 
	    {
	        JndiObjectFactoryBean bean = new JndiObjectFactoryBean();    
	        bean.setJndiName("java:/comp/env/jdbc/apiDS");
	        bean.setProxyInterface(DataSource.class);
	        bean.setLookupOnStartup(false);
	        bean.afterPropertiesSet();
	        
	        return (DataSource) bean.getObject();
	    }
}
