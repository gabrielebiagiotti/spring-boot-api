package it.dev.springbootapi.objects;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;


public class User implements Serializable{

	private int id;
	
	@NotBlank(message = "Name is mandatory")
	private String name;
	
	private String surname;
	
	private Date birthDate;

	public User() {
	}
	
	public User(int id, String name, String surname, Date birthDate) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.birthDate = birthDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
}
