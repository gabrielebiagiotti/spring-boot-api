package it.dev.springbootapi.objects;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class ObjectToValidate{

	@NotBlank(message = "first is mandatory")
	private String first;
	
	@Min(2)
	private Integer second;

}
