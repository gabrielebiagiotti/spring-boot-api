package it.dev.springbootapi.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import it.dev.springbootapi.objects.User;

@Service
public class UserService implements IUserService{

//	@Autowired
//	private DataSource dataSource;
	
	@Autowired
	@Qualifier("jndiDataSource")
	private DataSource dataSource;
	
	@Override
	public List<User> getUsers()
	{
		try (Connection con = dataSource.getConnection();
				PreparedStatement stmt = con.prepareStatement("SELECT * FROM USERS");
				ResultSet rs = stmt.executeQuery();) {
			List<User> users = new ArrayList<>();
			while (rs.next()) {
				users.add(new User(rs.getInt("ID"), rs.getString("NAME"), rs.getString("SURNAME"), rs.getDate("BIRTH_DATE")));
			}
			return users;
		} catch (Exception e) {
			System.err.println("Eccezione db:"+e);
			return null;
		}
	}
}
