package it.dev.springbootapi.services;

import java.util.List;

import it.dev.springbootapi.objects.User;

public interface IUserService {

	List<User> getUsers();

}
