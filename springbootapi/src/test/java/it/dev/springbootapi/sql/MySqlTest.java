package it.dev.springbootapi.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MySqlTest {

	@Test
	public void testConnection() {
		
		String driver = "com.mysql.cj.jdbc.Driver";
		String connection = "jdbc:mysql://85.10.205.173:3306/biagiomysql";
		String user = "biagiomysql";
		String password = "biagiomysql";
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.err.println(e);
			Assertions.fail();
		}
		try(Connection con = DriverManager.getConnection(connection, user, password);
				PreparedStatement stmt = con.prepareStatement("SELECT * FROM USERS");
				ResultSet rs = stmt.executeQuery();) {
			 List<String> users = new ArrayList<>();
			 while(rs.next())
			 {
				 String userData = rs.getInt("ID")+":"+rs.getString("NAME")+" "+rs.getString("SURNAME");
				System.out.println(userData);
				users.add(userData);
			 }
			 Assertions.assertTrue(users.size()>0);
			
		} catch (SQLException e) {
			System.err.println(e);
			Assertions.fail();
		}
	}

}
